# Install packages:

`sudo apt install resolvconf traceroute ntpstat ntp -y`

# Pull and run pre-checker

`git clone https://gitlab.com/blox1/precheck.git; sudo bash precheck/bloxoneprecheck`

# Run checks manually

- Install necessary packages
- Check for services listening on port 53

`sudo netstat -tunlp | grep '\:53\ '`

- If services are found stop services
- If `systemd-resolve` is found, change the DNS server to external server and disable install

    Install the resolvconf package.

        sudo apt install resolvconf

    Edit /etc/resolvconf/resolv.conf.d/head and add the following:

        # Make edits to /etc/resolvconf/resolv.conf.d/head.
        nameserver 8.8.4.4
        nameserver 8.8.8.8

    Restart the resolvconf service.

        sudo service resolvconf restart

- Validate connectivity to `52.119.40.100` over UDP port 53

`traceroute 52.119.40.100 -U -p 53`

- Validate connectivity to `52.119.40.100` over TCP port 443

`traceroute 52.119.40.100 -T -p 443`

- Check NTP status

`timedatectl status`

`ntpstat`

`ntpq -p`

