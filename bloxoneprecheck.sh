#!/bin/bash

echo -e "\nUbuntu release:\n---------------"
cat /etc/lsb-release

echo -e "\nLocal services listening on local port 53:\n------------------------------------------"
port53=$(netstat -tunlp | grep '\:53\ ')
if [[ $port53 ]]
then
        echo -e "\nFound service(s) listening on local port 53:\n"
        echo "$port53"
        echo -e "\nResolvers:\n\t"
        grep 'nameserver' /etc/resolv.conf
        echo "\nIf you have the systemd-resolve service listening on local port 53, follow the below steps:
        Install the resolvconf package.

	        sudo apt install resolvconf

        Edit /etc/resolvconf/resolv.conf.d/head and add the following:

                # Make edits to /etc/resolvconf/resolv.conf.d/head.
                nameserver 8.8.4.4
                nameserver 8.8.8.8

        Restart the resolvconf service.

	        sudo service resolvconf restart
        
        "
else
        echo -e "\nNo services listening on local port 53"
fi


echo -e "\nNTP status:\n-----------"
timedatectl status
if [[ $(dpkg -l | grep "ntpstat") ]]; then
        echo -e "\n"; ntpstat
fi
if [[ $(dpkg -l | grep ntp) ]]; then
        echo -e "\n"; ntpq -p
fi


echo -e "\n Traceroute on UDP port 53 to 52.119.40.100:\n--------------------------------------------"
traceroute 52.119.40.100 -U -p 53

echo  -e "\n Traceroute on TCP port 443 to 52.119.40.100:\n-------------------------------------------"
traceroute 52.119.40.100 -T -p 443
